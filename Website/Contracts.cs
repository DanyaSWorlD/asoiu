//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Website
{
    using System;
    using System.Collections.Generic;
    
    public partial class Contracts
    {
        public int Id { get; set; }
        public System.DateTime CreationDate { get; set; }
        public System.DateTime ExpireDate { get; set; }
        public int RentTime { get; set; }
        public int Costs { get; set; }
        public Nullable<int> Model { get; set; }
        public Nullable<int> User { get; set; }
    
        public virtual Models Models { get; set; }
        public virtual Users Users { get; set; }
    }
}
