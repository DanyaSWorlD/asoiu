﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="admin.aspx.cs" Inherits="Website.admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .style1 {
            text-align: center
        }
    </style>
    Модели:<asp:Button ID="Button1" runat="server" Text="Добавить модель" OnClick="Button1_Click" />&nbsp;<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="ModelsDataSource" ForeColor="#333333" GridLines="None" AllowPaging="True" AllowSorting="True" ClientIDMode="Static" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" OnRowDeleting="GridView1_RowDeleting">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True" />
            <asp:BoundField DataField="Id" HeaderText="Номер" />
            <asp:BoundField DataField="Manufacturer" HeaderText="Производитель" SortExpression="Manufacturer" />
            <asp:BoundField DataField="Model" HeaderText="Модель" SortExpression="Model" />
            <asp:BoundField DataField="Price" HeaderText="Цена" SortExpression="Price" />
            <asp:BoundField DataField="ModelTypes.Type" HeaderText="Тип" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" CssClass="style1" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:ObjectDataSource ID="ModelsDataSource" runat="server" DataObjectTypeName="Website.Models" DeleteMethod="DeleteModels" InsertMethod="InsertModels" SelectMethod="GetModels" TypeName="Website.admin" UpdateMethod="UpdateModels"></asp:ObjectDataSource>
    <br />


    Выезды:
    <asp:Button ID="Button2" runat="server" Text="Добавить выезд" OnClick="Button2_Click" />
    <br />
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="RidesDataSource" ForeColor="#333333" GridLines="None" OnRowCancelingEdit="GridView2_RowCancelingEdit" OnRowEditing="GridView2_RowEditing" OnRowUpdating="GridView2_RowUpdating">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" Visible="False" />
            <asp:BoundField DataField="StartTime" HeaderText="Время начала" SortExpression="StartTime" />
            <asp:BoundField DataField="Status" HeaderText="Состояние" SortExpression="Status" />
            <asp:BoundField DataField="Model" HeaderText="Номер модели" SortExpression="Model" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" CssClass="style1" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <br />
    <asp:ObjectDataSource ID="RidesDataSource" runat="server" SelectMethod="GetRides" TypeName="Website.admin" DataObjectTypeName="Website.Rides" DeleteMethod="DeleteRides" UpdateMethod="UpdateRides"></asp:ObjectDataSource>
    Контракты:
    <asp:Button ID="Button3" runat="server" Text="Добавить контракт" OnClick="Button3_Click" />
    <br />
    <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCancelingEdit="GridView3_RowCancelingEdit" OnRowEditing="GridView3_RowEditing" OnRowUpdating="GridView3_RowUpdating" DataSourceID="ContractsDataSource1">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" Visible="False" />
            <asp:BoundField DataField="CreationDate" HeaderText="Дата создания" SortExpression="CreationDate" />
            <asp:BoundField DataField="ExpireDate" HeaderText="Дата завершения" SortExpression="ExpireDate" />
            <asp:BoundField DataField="RentTime" HeaderText="Время аренды (в часах)" SortExpression="RentTime" />
            <asp:BoundField DataField="Costs" HeaderText="Стоимость" SortExpression="Costs" />
            <asp:BoundField DataField="Model" HeaderText="Номер модели" SortExpression="Model" />
            <asp:BoundField DataField="User" HeaderText="Индекс пользователя" SortExpression="User" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" CssClass="style1" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:ObjectDataSource ID="ContractsDataSource1" runat="server" DataObjectTypeName="Website.Contracts" DeleteMethod="DeleteContracts" SelectMethod="GetContracts" TypeName="Website.admin" UpdateMethod="UpdateContracts"></asp:ObjectDataSource>
    <br />
    Дефекты:
    <asp:Button ID="Button4" runat="server" Text="Добавить дефект" OnClick="Button4_Click" />
    <br />
    <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCancelingEdit="GridView4_RowCancelingEdit" OnRowEditing="GridView4_RowEditing" OnRowUpdating="GridView4_RowUpdating" DataSourceID="DefectsDataSource">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" Visible="False" />
            <asp:BoundField DataField="Part" HeaderText="Деталь" SortExpression="Part" />
            <asp:BoundField DataField="Value" HeaderText="Серьезность" SortExpression="Value" />
            <asp:BoundField DataField="Comment" HeaderText="Комментарий" SortExpression="Comment" />
            <asp:BoundField DataField="Model" HeaderText="Номер модели" SortExpression="Model" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" CssClass="style1" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:ObjectDataSource ID="DefectsDataSource" runat="server" DataObjectTypeName="Website.Defects" DeleteMethod="DeleteDefects" SelectMethod="GetDefects" TypeName="Website.admin" UpdateMethod="UpdateDefects"></asp:ObjectDataSource>
    <br />
</asp:Content>
